;;; leaves.el --- Advanced support for paginated text in Emacs -*- lexical-binding: t -*-

;; Package-Requires: ((hydra "0.13.4"))
;; Author: Walter Lewis
;; URL: https://codeberg.org/warp/leaves
;; Keywords: pages

;; This file is part of leaves.
;;
;; leaves is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; leaves is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; leaves. If not, see <http://www.gnu.org/licenses/>.
;;; Code:

(require 'hydra)

(defgroup leaves nil
  "Customization options for `leaves-mode'."
  :group 'convenience)

(defcustom leaves-delimiter
  page-delimiter
  "Regular expression matching leaf delimiters in `leaves-mode'."
  :type '(choice (const :tag "Page" "^")
                 (regexp :tag "Custom"))
  :group 'leaves)

(defcustom leaves-auto-enable-overlay nil
  "Non-nil if enabling `leaves-mode' should also enable an overlay."
  :type 'boolean
  :group 'leaves)

(defcustom leaves-overlay-face-attributes
  '(:weight bold)
  "Face attributes applied to the leaf overlay."
  :type '(plist)
  :group 'leaves)

(defvar leaves-buffer nil)
(defvar leaves-leaf nil)
(defvar leaves-narrow-p nil)
(defvar leaves-overlay nil)
(defvar leaves--prev-point nil)

(defvar leaves-mode-map (make-sparse-keymap))

;; common

;;;###autoload
(define-minor-mode leaves-mode
  "Minor mode for working with paginated text."
  :init-value nil
  :lighter " Leaves"
  :keymap leaves-mode-map
  (setq leaves-leaf nil)
  (if leaves-mode
      (progn
        (setq leaves-buffer (current-buffer))
        (leaves--update-leaf)
        (when leaves-auto-enable-overlay
          (leaves-toggle-overlay-p 1)))
    (leaves--update-header)
    (leaves-sync-mode -1)
    (leaves-toggle-narrow-p -1)
    (leaves-toggle-overlay-p -1)))

(defun leaves-live-p ()
  (bound-and-true-p leaves-mode))

(defun leaves-sync-live-p ()
  (and (bound-and-true-p leaves-sync-mode)
       (not leaves-sync-frozen-p)
       (or (null leaves-sync-buffer)
           (buffer-live-p leaves-sync-buffer))))

(defhydra hydra-leaves-motion (:hint nil)
  "
[next/prev]: [_n_/_p_] line [_f_/_b_] leaf [_e_/_E_] end _._ reload _c_ center | _g_ goto | _v_ visual | _r_ restrict | _s_ sync"
  ("n" leaves-turn-forward-line)
  ("p" leaves-turn-backward-line)
  ("f" leaves-turn-forward-leaf-begin)
  ("b" leaves-turn-backward-leaf-begin)
  ("e" leaves-turn-forward-leaf-end)
  ("E" leaves-turn-backward-leaf-end)
  ("." leaves-turn-in-place)
  ("g" leaves-turn-to-leaf)
  ("c" leaves-recenter)
  ("v" hydra-leaves-visual/body :exit t)
  ("r" hydra-leaves-narrow/body :exit t)
  ("s" hydra-leaves-sync/body :exit t))

(defhydra hydra-leaves-visual
  (:body-pre (leaves-toggle-mark))
  "visual"
  ("v" leaves-toggle-mark "toggle mark")
  ("m" hydra-leaves-motion/body "motion" :exit t))

(defhydra hydra-leaves-narrow
  (:body-pre (leaves-toggle-narrow-p))
  "restrict"
  ("r" leaves-toggle-narrow-p "toggle restricted")
  ("m" hydra-leaves-motion/body "motion" :exit t))

(define-key leaves-mode-map
  (kbd "C-c C-v") 'hydra-leaves-motion/body)

(defun leaves--toggle (arg pred fn)
  "Apply (not PRED) to FN so long as it matches the sign of ARG.
FN should be a procedure that takes a boolean argument.

Matches the behavior of interactive toggles like minor modes."
  (if pred
      (unless (and arg (> (prefix-numeric-value arg) 0))
        (funcall fn nil))
    (unless (and arg (< (prefix-numeric-value arg) 0))
      (funcall fn t))))

(defun leaves--delimiter-regexp ()
  (concat leaves-delimiter "\\s-?"))

;; navigation

;;;###autoload
(defun leaves-turn-in-place ()
  "Re-turn the current leaf."
  (interactive)
  (leaves-turn #'set 'leaves-leaf nil))

;;;###autoload
(defun leaves-turn-forward-line (count)
  (interactive "p")
  (leaves-turn #'forward-line count))

;;;###autoload
(defun leaves-turn-backward-line (count)
  (interactive "p")
  (leaves-turn #'forward-line (- count)))

;;;###autoload
(defun leaves-turn-to-leaf (leaf)
  "Turn to beginning of LEAF, read from minibuffer."
  (interactive "nLeaf: ")
  (leaves-turn #'leaves-beginning-of-leaf leaf))

;;;###autoload
(defun leaves-turn-forward-leaf-begin (count)
  "Turn to next leaf beginning. Repeat COUNT times."
  (interactive "p")
  (if (< count 0)
      (leaves-turn-backward-leaf-begin (- count))
    (leaves-turn #'leaves--forward-leaf count)))

;;;###autoload
(defun leaves-turn-backward-leaf-begin (count)
  "Turn to previous leaf beginning. Repeat COUNT times."
  (interactive "p")
  (if (< count 0)
      (leaves-turn-forward-leaf-begin (- count))
    (leaves-turn
     (lambda ()
       (leaves--backward-delim)
       (leaves--forward-leaf (- count))
       (leaves--forward-delim)))))

;;;###autoload
(defun leaves-turn-forward-leaf-end (count)
  "Turn to next leaf end. Repeat COUNT times."
  (interactive "p")
  (if (< count 0)
      (leaves-turn-backward-leaf-end (- count))
    (leaves-turn
     (lambda ()
       (leaves--forward-delim)
       (leaves--forward-leaf count)
       (leaves--backward-delim)))))

;;;###autoload
(defun leaves-turn-backward-leaf-end (count)
  "Turn to previous leaf end. Repeat COUNT times."
  (interactive "p")
  (if (< count 0)
      (leaves-turn-forward-leaf-end (- count))
    (leaves-turn #'leaves--forward-leaf (- count))))

;;;###autoload
(defun leaves-turn-to-end (sign)
  "Turn to the end of the last leaf and return point.
With negative prefix SIGN, turn to `point-min' instead."
  (interactive "p")
  (leaves-turn #'leaves--end-of-book sign))

(defun leaves-beginning-of-leaf (&optional leaf)
  "Go to leaf beginning and return point.
With LEAF non-nil, go to beginning of LEAF."
  (leaves--enter-book)
  (let ((delta (- (or leaf leaves-leaf) leaves-leaf)))
    (leaves--forward-leaf delta)
    (if (> delta 0) (point)
      (leaves--backward-leaf)
      (leaves--forward-delim))))

(defun leaves-end-of-leaf (&optional leaf)
  "Go to leaf end and return point.
With LEAF non-nil, go to end of LEAF."
  (leaves--enter-book)
  (let ((delta (- (or leaf leaves-leaf) leaves-leaf)))
    (leaves--forward-leaf delta)
    (if (< delta 0) (point)
      (leaves--forward-leaf)
      (leaves--backward-delim))))

(defun leaves--forward-leaf (&optional count)
  "Move forward COUNT leaf beginnings and return point."
  (setq count (or count 1))
  (unless (= count 0)
    (if (< count 0)
        (leaves--backward-leaf (- count))
      (let ((bound (save-excursion (leaves--end-of-book))))
        (goto-char
         (or (and (< (point) bound)
                  (re-search-forward (leaves--delimiter-regexp) bound t count))
             bound))))))

(defun leaves--backward-leaf (&optional count)
  "Move backward COUNT leaf ends and return point."
  (setq count (or count 1))
  (unless (= 0 count)
    (if (< count 0)
        (leaves--forward-leaf (- count))
      (let ((bound (point-min)))
        (goto-char
         (or (re-search-backward (leaves--delimiter-regexp) bound t count)
             bound))))))

(defun leaves--end-of-book (&optional sign)
  "Move to the end of the last leaf and return point.
With negative prefix SIGN, turn to `point-min' instead."
  (if (and sign (< sign 0))
      (goto-char (point-min))
    (goto-char (point-max))
    (leaves--backward-leaf)))

(defun leaves--enter-book ()
  "Move point to beginning of book, unless already inside."
  (unless leaves-leaf
    (goto-char (point-min))
    (setq leaves-leaf 0)))

(defun leaves--forward-delim ()
  "Move forward a single delim and return point.
If not looking at a delim, do nothing."
  (goto-char
   (if (looking-at
        (leaves--delimiter-regexp))
       (match-end 0)
     (point))))

(defun leaves--backward-delim ()
  "Move backward a single delim and return point.
If not looking back at a delim, do nothing."
  (goto-char
   (if (looking-back
        (leaves--delimiter-regexp)
        (save-excursion (beginning-of-line -1) (point)))
       (match-beginning 0)
     (point))))

;; edit

;;;###autoload
(defun leaves-toggle-mark (&optional arg)
  "Toggle the mark, snapping to the previous `leaves-delimiter'."
  (interactive "P")
  (leaves--set-narrow-p nil)
  (leaves--toggle arg mark-active #'leaves--set-mark))

(defun leaves--set-mark (active)
  (if active
      (push-mark (leaves-beginning-of-leaf) nil t)
    (pop-mark)))

;;;###autoload
(defun leaves-push-forward (count &optional begin end)
  "Move region or leaf text after point COUNT leaves forward."
  (interactive "p\nr")
  (if (and (use-region-p)
           begin end)
      (leaves-turn #'leaves-move begin end #'leaves--forward-leaf count)
    (leaves-turn #'leaves--move-delim count)))

;;;###autoload
(defun leaves-push-backward (count &optional begin end)
  "Move region or leaf text before point COUNT leaves backward."
  (interactive "p\nr")
  (if (and (use-region-p)
           begin end)
      (leaves-turn #'leaves-move begin end #'leaves--backward-leaf count)
    (leaves-turn #'leaves--move-delim (- count))))

(defun leaves--move-delim (&optional count)
  (delete-horizontal-space)
  (insert "\n" (save-excursion (leaves--kill-delim count))))

(defun leaves--kill-delim (&optional count)
  (let* ((region (leaves--mark-delim count))
         (start (car region))
         (end (cdr region))
         (str (buffer-substring start end)))
    (delete-region start end)
    str))

(defun leaves--mark-delim (&optional count)
  (when (leaves--forward-leaf count)
    (cons (match-beginning 0) (match-end 0))))

;; view

;;;###autoload
(defun leaves-recenter ()
  (interactive)
  (save-excursion
    (leaves-beginning-of-leaf)
    (recenter 0)))

;;;###autoload
(defun leaves-toggle-narrow-p (&optional arg)
  "Toggle narrowing of the leaf at point.
With numeric prefix argument ARG, enable if positive or disable
if negative."
  (interactive "P")
  (leaves--set-mark nil)
  (leaves--toggle arg leaves-narrow-p #'leaves--set-narrow-p))

(defun leaves--set-narrow-p (active)
  (setq leaves-narrow-p active)
  (if active
      (progn
        (narrow-to-region
         (save-excursion (leaves-beginning-of-leaf))
         (save-excursion (leaves-end-of-leaf)))
        (when (leaves-sync-live-p)
          (leaves-sync-run 'play)))
    (widen)
    (when (leaves-sync-live-p)
      (leaves-sync-run 'pause)))
  (leaves--update-header))

(defun leaves-turn (fn &rest args)
  "Widen, apply FN with ARGS, and narrow.
Simply apply FN with ARGS if not narrowed."
  (let ((narrow-p leaves-narrow-p))
    (when narrow-p
      (leaves-toggle-narrow-p -1))
    (apply fn args)
    (leaves--update-leaf)
    (when narrow-p
      (leaves-toggle-narrow-p 1))))

(defun leaves-move (begin end fn &rest args)
  "Kill the text between BEGIN and END, apply FN with ARGS, and yank.
Simply apply FN with ARGS if region is not in use."
  (let ((regionp (use-region-p)))
    (when regionp
      (kill-region begin end))
    (when (looking-at "\\s-*$")
      (delete-blank-lines))
    (apply fn args)
    (when regionp
      (yank))))

;;;###autoload
(defun leaves-toggle-overlay-p (&optional arg)
  "Toggle overlay highlighting of the master leaf at point.
With numeric prefix argument ARG, enable if positive or disable
if negative."
  (interactive "P")
  (leaves--toggle arg (overlayp leaves-overlay) #'leaves--set-overlay-p))

(defun leaves--set-overlay-p (active)
  (if active
      (overlay-put
       (setq leaves-overlay
             (make-overlay
              (save-excursion (leaves-beginning-of-leaf))
              (save-excursion (leaves-end-of-leaf))
              nil nil t))
       'face
       leaves-overlay-face-attributes)
    (setq leaves-overlay
          (delete-overlay leaves-overlay))))

;; leaf access

(defun leaves--count-leaf ()
  "Return the leaf number around point, delimited by `leaves-delimiter'.
The first leaf is leaf 0."
  (let ((bound (point)))
    (when (re-search-forward leaves-delimiter nil t)
      (goto-char (point-min))
      (leaves--count-delims bound))))

(defun leaves--count-delims (bound)
  (let ((count 0))
    (cl-loop while (re-search-forward leaves-delimiter bound 'lim)
             do (setq count (1+ count)))
    count))

(defun leaves-get-leaf-string (&optional leaf)
  "Return a substring of the current leaf, or of LEAF."
  (save-excursion
    (buffer-substring
     (leaves-beginning-of-leaf leaf)
     (leaves-end-of-leaf))))

(defun leaves-get-leaf-sample (&optional leaf)
  "Extract the first line of the current leaf, or of LEAF."
  (save-excursion
    (buffer-substring-no-properties
     (progn
       (leaves-beginning-of-leaf leaf)
       (skip-syntax-forward " ")
       (point))
     (line-end-position))))

;; leaf tracking

(defun leaves--update-leaf ()
  "Calculate `leaves-leaf' and call any necessary updates.
Called as part of `post-command-hook'."
  (when (and (leaves-live-p)
             (not leaves-narrow-p)
             (or (null leaves-leaf)
                 (null leaves--prev-point)
                 (/= leaves--prev-point (point))))
    (setq leaves--prev-point (point))
    (let ((new-leaf (leaves--count-leaf)))
      (when (or (null leaves-leaf)
                (null new-leaf)
                (/= leaves-leaf new-leaf))
        (when (setq leaves-leaf new-leaf)
          (leaves--update-overlay)
          (when (leaves-sync-live-p)
            (condition-case err
                (leaves-sync-pos)
              (error
               (message (error-message-string err))))))
        (leaves--update-header)))))

(add-hook 'post-command-hook #'leaves--update-leaf)

(defun leaves--update-header ()
  "Update `header-line-format' to indicate the current leaf."
  (setq header-line-format
        (when leaves-leaf
          (format "Leaf %s %s%s %s"
                  leaves-leaf
                  (if leaves-narrow-p "NARR " "")
                  (if (bound-and-true-p leaves-sync-mode)
                      (if leaves-sync-frozen-p
                          "FRZN"
                        "SYNC ")
                    "")
                  (leaves-get-leaf-sample)))))

(defun leaves--update-overlay ()
  "Move `leaves-overlay' to the current leaf."
  (when (overlayp leaves-overlay)
    (move-overlay
     leaves-overlay
     (save-excursion (leaves-beginning-of-leaf))
     (save-excursion (leaves-end-of-leaf)))))

;; file

;;;###autoload
(defun leaves-normalize-delims ()
  "Ensure each `leaves-delimiter' is on its own line."
  (interactive)
  (goto-char (point-min))
  (let ((count 0))
    (while (re-search-forward (concat "\\(" leaves-delimiter "\\)\\(.\\)") nil t)
      (setq count (1+ count))
      (replace-match "\\1\n\\2")
      (backward-char))
    (message "Normalized %d delimiters" count)))

;;;###autoload
(defun leaves-export (start end)
  "Write the current region to a file, removing `leaves-delimiter's."
  (interactive
   (if (use-region-p)
       (list (region-beginning) (region-end))
     (list (point-min) (point-max))))
  (goto-char start)
  (let ((leaf (leaves--count-leaf))
        (str (buffer-substring (point) end))
        (filename (read-file-name "Write to file:"))
        (stamp-p (and (leaves-sync-live-p)
                      (y-or-n-p "Insert position markers?"))))
    (unless (or (not (file-exists-p filename))
                (y-or-n-p (format "%s exists, replace?" filename)))
      (error "Cancelled"))
    (with-temp-buffer
      (insert str)
      (goto-char (point-min))
      (while (re-search-forward (concat leaves-delimiter "\\s-?") nil t)
        (replace-match
         (if (not stamp-p) ""
           (format "[%s]\n" (leaves-sync-leaf->pos leaf))))
        (setq leaf (1+ leaf)))
      (write-region (point-min) (point-max) filename))))

(provide 'leaves)

;;; leaves.el ends here
